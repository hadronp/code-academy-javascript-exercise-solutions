// if / else
var isEven = function(number) {
  // Your code goes here!
  if(number % 2 === 0){
  	return true;
  }
  else{
  	return false;
  }
};



// if / else if / else
var isEven = function(number) {
  // Your code goes here!
  if (isNaN(number)){
  	return "Input is not a number";
  }
  else if(number % 2 === 0){
  	return true;
  }
  else{
  	return false;
  }
};




// For or while
var x=10;
while(x>0){
	console.log("Counting: "+ x);
	x-=1;
}




//Switch statement
var color = prompt("What's your favorite primary color?","Type your favorite color here");

switch(color) {
  case 'red':
    console.log("Red's a good color!");
    break;
  case 'blue':
    console.log("That's my favorite color, too!");
    break;
  //Add your case here!
  case 'yellow':
  	console.log("Bright and sunny color!");
  	break;
  default:
    console.log("I don't think that's a primary color!");
}




//Practice with switch
var candy = prompt("What's your favorite candy?","Type your favorite candy here.");

switch(candy) {
  case 'licorice':
    console.log("Gross!");
    break;
  case 'gum':
    console.log("I like gum!");
    break;
  case 'beets':
    console.log("...is that even a candy?");
    break;
  // Add your code here!
  default:
  	console.log("Default switch case");
  
}




// More practice with switch
var answer = prompt("What is your race in Dungeon Crawl?");

switch(answer) {
  case 'minotaur':
    console.log("you like smashing and head butting");
    break;
  // Add your code here!
  case 'Troll':
  	console.log("brute force is cool, use a massive spiked club");
  	break;
  default:
  	console.log("try playing Dugeon Crawl");
}


//All on your own
var answer = prompt("What is your race in Dungeon Crawl?");

switch(answer) {
  case 'minotaur':
    console.log("you like smashing and head butting");
    break;
  // Add your code here!
  case 'troll':
  	console.log("brute force is cool, use a massive spiked club");
  	break;
  case 'centaur':
  	console.log("you love bows and fancy kicking orcs");
  	break;
  default:
  	console.log("try playing Dugeon Crawl");
}





//Logical operators overview
// Complete lines 3 and 4!

var iLoveJavaScript = true;
var iLoveLearning = true;

if(iLoveJavaScript && iLoveLearning) {
  // if iLoveJavaScript AND iLoveLearning:
  console.log("Awesome! Let's keep learning!");
} else if(!(iLoveJavaScript || iLoveLearning)) {
  // if NOT iLoveJavaScript OR iLoveLearning:
  console.log("Let's see if we can change your mind.");
} else {
  console.log("You only like one but not the other? We'll work on it.");
}





// And
// Declare your variables here!
var hungry=true;
var foodHere= true;

var eat = function() {
  // Add your if/else statement here!
  if(hungry && foodHere){
  	return true;
  }
  else{
  	return false;
  }
};





// Or
// Declare your variables here!
var tired= true;
var bored= false;

var nap = function() {
  // Add your if/else statement here!
  if (tired || bored){
  	return true;
  }
  else{
  	return false;
  }
};




// Not
// Declare your variables here!
var programming=false;

var happy = function() {
  // Add your if/else statement here!
  if (programming){
  	return false;
  }
  else{
  	return true;
  }
  
};