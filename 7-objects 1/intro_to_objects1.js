// We've come a long, long...
var answer=(((3 * 90) === 270) || !(false && (!false)) || "bex".toUpperCase() === "BEX");



//Through the hard times...
// Here is an array of multiples of 8. But is it correct?
var multiplesOfEight = [8,16,24,32,40,58];

// Test to see if a number from the array is NOT a true
// multiple of eight. Real multiples will return false.
var answer = multiplesOfEight[16] % 8 !== 0;



//...And the good!
for (var i=1; i<=20; i++){
	if (i % 3 ===0 && i % 5 === 0 ){
		console.log("FizzBuzz");
	}

	else if (i % 3 === 0){
		console.log("Fizz");
	}
	else if (i % 5 === 0){
		console.log("Buzz");
	}
	
	else{
		console.log(i);
	}
}




//I have to celebrate you baby
var getReview = function (movie) {
	switch(movie){
		case "Toy Story 2":
			return "Great story. Mean prospector.";
			break;
		
		case "Finding Nemo":
			return "Cool animation, and funny turtles.";
			break;

		case "The Lion King":
			return "Great songs."
			break;
		
		default:
			return "I don't know";
	}

};




//I have to praise you like I should!
console.log("I'm ready for objects!");


//Intro
var bob={};


//Properties
var Spencer = {
  age: 22,
  country: "United States"
};

// make your own object here called me
var me={
	age: 33,
	country: "Philippines"
};



// Accessing Properties
var bob = {
  name: "Bob Smith",
  age: 30
};

var susan = {
  name: "Susan Jordan",
  age: 25
};

// here we save Bob's information
var name1 = bob.name;
var age1 = bob.age;
// finish this code by saving Susan's information
var name2 =  susan.name;
var age2 =	susan.age;




//Accessing Properties, Part 2
// Take a look at our next example object, a dog
var dog = {
  species: "greyhound",
  weight: 60,
  age: 4
};

var species = dog["species"];
// fill in the code to save the weight and age using bracket notation
var weight = dog.weight;
var age =	dog.age;



//Another Way to Create
// Our bob object again, but made using a constructor this time 
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;
// Here is susan1, in literal notation

var susan1 = {
  name: "Susan Jordan",
  age: 24
};
// Make a new susan2 object, using a constructor instead
var susan2= new Object();
susan2=susan1;




//Putting it all together
// help us make snoopy using literal notation
// Remember snoopy is a "beagle" and is 10 years old.
var snoopy ={
	species: "beagle",
	age: 10
};

// help make buddy using constructor notation
// buddy is a "golden retriever" and is 5 years old
var buddy = new Object();
buddy.species="golden retriever";
buddy.age=5;




// More Practice Making Objects
var bicycle={
	speed: 0,
	gear: 1,
	frame_material: "carbon fiber"
};




//Function Review
// Accepts a number x as input and returns its square
var square = function (x) {
  return x * x;
};

// Write the function multiply below
// It should take two parameters and return the product
var multiply= function(x,y){
	return x * y;

};

console.log(multiply(2,3));




//So What's a Method?
// here is bob again, with his usual properties
var bob = new Object();
bob.name = "Bob Smith";
bob.age = 30;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};
// here we set bob's age to 40
bob.setAge(40);
// bob's feeling old.  Use our method to set bob's age to 20
bob.setAge(20);




// Why Are Methods Important?
var bob = new Object();
bob.age = 17;
// this time we have added a method, setAge
bob.setAge = function (newAge){
  bob.age = newAge;
};

bob.getYearOfBirth = function () {
  return 2014 - bob.age;
};
console.log(bob.getYearOfBirth());




// The "this" keyword
// here we define our method using "this", before we even introduce bob
var setAge = function (newAge) {
  this.age = newAge;
};
// now we make bob
var bob = new Object();
bob.age = 30;
// and down here we just use the method we already made
bob.setAge = setAge;
  
// change bob's age to 50 here
bob.setAge(50);




//"This" Works for Everyone
// here we define our method using "this", before we even introduce bob
var setAge = function (newAge) {
  this.age = newAge;
};
// now we make bob
var bob = new Object();
bob.age = 30;
bob.setAge = setAge;
  
// make susan here, and first give her an age of 25
var susan=new Object();
susan.age= 25;
susan.setAge=setAge;


// here, update Susan's age to 35 using the method
susan.setAge(35);




//Make Your Own Method
var rectangle = new Object();
rectangle.height = 3;
rectangle.width = 4;
// here is our method to set the height
rectangle.setHeight = function (newHeight) {
  this.height = newHeight;
};

// help by finishing this method
rectangle.setWidth = function(newWidth){
	this.width= newWidth;
};
  

// here change the width to 8 and height to 6 using our new methods
rectangle.setWidth(8);
rectangle.setHeight(6);




//More Kinds of Methods
var square = new Object();
square.sideLength = 6;
square.calcPerimeter = function() {
  return this.sideLength * 4;
};
// help us define an area method here
square.calcArea= function(sideLength){
	return this.sideLength * this.sideLength;
};

var p = square.calcPerimeter();
var a = square.calcArea();



// The Object Constructor
// here we make bob using the Object constructor
var bob = new Object();
bob.name = "Bob Smith";
// add bob's age here and set it equal to 20
bob.age=20;




// Custom Constructors
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Let's make bob and susan again, using our constructor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 25);
// help us make george, whose name is "George Washington" and age is 275
var george=new Person("George Washington", 275);





// Try it out
function Cat(age, color) {
  this.age = age;
  this.color = color;
}

// make a Dog constructor here
function Dog(age,color, temperament){
	this.age=age;
	this.color= color;
	this.temperament= temperament;
}




// More Options
function Person(name,age) {
  this.name = name;
  this.age = age;
  this.species = "Homo Sapiens";
}

var sally = new Person("Sally Bowles", 39);
var holden = new Person("Holden Caulfield", 16);
console.log("sally's species is " + sally.species + " and she is " + sally.age );
console.log("holden's species is " + holden.species + " and he is " + holden.age);




// Constructors with methods
function Rectangle(height, width) {
	//var self = this;

  this.height = height;
  this.width = width;
  this.calcArea = function() {
  	return this.height * this.width;
  };
  // put our perimeter function here!
  this.calcPerimeter= function(height, width){
  	return 2 * this.height + 2 * this.width;
  };
  
}

var rex = new Rectangle(7,3);
var area = rex.calcArea();
var perimeter = rex.calcPerimeter();



// Constructors in Review
function Rabbit(adjective) {
    this.adjective = adjective;
    this.describeMyself = function() {
        console.log("I am a " + this.adjective + " rabbit");
    };
}

// now we can easily make all of our rabbits
var rabbit1= new Rabbit("fluffy");
var rabbit2= new Rabbit("happy");
var rabbit3= new Rabbit("sleepy");

rabbit1.describeMyself;
rabbit2.describeMyself;
rabbit3.describeMyself;




//Arrays of Objects
// Our person constructor
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// Now we can make an array of people
var family = new Array();
family[0] = new Person("alice", 40);
family[1] = new Person("bob", 42);
family[2] = new Person("michelle", 8);
// add the last family member, "timmy", who is 6 years old
family[3]= new Person("timmy",6);




//Loop the loop
// Our Person constructor
function Person(name, age){
	this.name=name;
	this.age=age;
}

// Now we can make an array of people
var family= new Array();

family[0]= new Person("alice",40);
family[1]= new Person("bob",42);
family[2]= new Person("michelle",8);
family[3]= new Person("timmy",6);

// loop through our new array
for (i=0; i< family.length; i++){
	console.log(family[i].name);
}




//Passing Objects into functions
// Our person constructor
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// We can make a function which takes persons as arguments
// This one computes the difference in ages between two people
var ageDifference = function(person1, person2) {
    return person1.age - person2.age;
}

var alice = new Person("Alice", 30);
var billy = new Person("Billy", 25);

// get the difference in age between alice and billy using our function
var diff = ageDifference(alice,billy);




// Try it out
// Our person constructor
function Person (name, age) {
    this.name = name;
    this.age = age;
}

// We can make a function which takes persons as arguments
// This one computes the difference in ages between two people
var ageDifference = function(person1, person2) {
    return person1.age - person2.age;
};

// Make a new function, olderAge, to return the age of
// the older of two people
var olderAge= function(person1, person2){
	if (person1.age > person2.age){
		return person1.age;
	}
	else if (person2.age > person1.age){
		return person2.age;
	}
	else{
		return person1.age;
	}
};

// Let's bring back alice and billy to test our new function
var alice = new Person("Alice", 30);
var billy = new Person("Billy", 25);

console.log("The older person is " + olderAge(alice, billy));



//What are objects for
var spencer = {
  age: 22,
  country: "United States"
};

// make spencer2 here with constructor notation
var spencer2 = new Object();
spencer2.age = 22;
spencer2.country = "United States";



//Properties
var snoopy = new Object();
snoopy.species = "beagle";
snoopy.age = 10;

// save Snoopy's age and species into variables
// use dot notation for snoopy's species
var species = snoopy.species;
    
// use bracket notation for snoopy's age
var age = snoopy["age"];



//Customizing Constructors
// 3 lines required to make harry_potter
var harry_potter = new Object();
harry_potter.pages = 350;
harry_potter.author = "J.K. Rowling";

// A custom constructor for book
function Book (pages, author) {
    this.pages = pages;
    this.author = author;
}

// Use our new constructor to make the_hobbit in one line
var the_hobbit=new Book(320,"J.R.R. Tolkien");




//Methods
function Circle (radius) {
    this.radius = radius;
    this.area = function () {
        return Math.PI * this.radius * this.radius;
        
    };
    // define a perimeter method here
    this.perimeter= function(){
    	return 2 * Math.PI * this.radius;
    };
    
};