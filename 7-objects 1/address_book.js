var bob = {
    firstName: "Bob",
    lastName: "Jones",
    phoneNumber: "(650) 777-777",
    email: "bob.jones@example.com"
};

console.log(bob.firstName);
console.log(bob.lastName);
console.log(bob.email);

var mary= {
	firstName: "Mary",
	lastName: "Johnson",
	phoneNumber: "(650) 888 - 8888",
	email: "mary.johnson@example.com"
};

var contacts = [bob, mary];

console.log(mary.phoneNumber);

var printPerson= function(person){
	 console.log(person.firstName + " " + person.lastName);
};

console.log(printPerson(contacts[0]));
console.log(printPerson(contacts[1]));

var list=function(){
	var contactsLength=contacts.length;

	for(i=0; i< contacts.length; i++){
		printPerson(contacts[i]);
	}
};


var search= function(lastName){
	var contactsLength=contacts.length;
	for(i=0 ;i < contacts.length; i++){
		if(contacts[i].lastName === lastName){
			printPerson(contacts[i]);
		}
	}
};


var add= function(firstName, lastName, email, phoneNumber){
	var newContact= new Object();
	newContact.firstName = firstName;
	newContact.lastName = lastName;
	newContact.email = email;
	newContact.phoneNumber = phoneNumber;
	
	contacts[contacts.length] = newContact;
};

console.log(list());
search("Jones");

add("Trollus", "Impaktus", "troll@gmail.com","1234-5678");
console.log(list());



