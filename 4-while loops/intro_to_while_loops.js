var coinFace = Math.floor(Math.random() * 2);

while(coinFace === 0){
	console.log("Heads! Flipping again...");
	var coinFace = Math.floor(Math.random() * 2);
}
console.log("Tails! Done flipping.");



// While syntax
var understand = true;

while(understand === true ){
	console.log("I'm learning while loops!");
	understand = false;
}



//A fellow of infinite loops
understand = true;

while(understand){
	console.log("I'm learning while loops!");
	//Change the value of 'understand' here!
	understand= false;
}




//Brevity is the soul of programming
var bool = true;

while(bool){
    console.log("Less is more!");
    bool = false;
}




//Practice makes perfect
//Remember to set your condition outside the loop!
var loop = function(){
	var count=3;
	while(count>0){
		//Your code goes here!
		console.log("I'm looping!");
		count--;
	}
};

loop();




//Solo flight
//Remember to make your condition true outside the loop!
var soloLoop = function(){
  var condition=true;
  //Your code goes here!
  while(condition){
  	console.log("Looped once!");
  	condition=false;
  }
  
};

soloLoop();



//When to 'while' and when to 'for'
var count=5;
while(count > 0){
	console.log("Looping "+ count);
	count--;
}

for (var i=5; i>0; i--){
	console.log("Looping "+ i);
}




//The 'do' / 'while' loop
var loopCondition = false;

do {
	console.log("I'm gonna stop looping 'cause my condition is " + loopCondition + "!");	
} while (loopCondition);




//To learn it, you gotta do it
var getToDaChoppa = function (name){
  // Write your do/while loop here!
  var loop=false;
  do{
  	console.log("Hello "+ name);
  } while(loop);
  
};

getToDaChoppa("Minotaur");




//Review
for(var i=100; i>0; i-=10){
	console.log(i);
}

x=100
while(x>0){
	console.log(x);
	x-=10;
}

j=100
do{
	console.log(j);
	j-=10;
} while(j>0);