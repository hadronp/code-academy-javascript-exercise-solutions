// Why use for loops
console.log(1);
console.log(2);
console.log(3);
console.log(4);
console.log(5);



//1st for loop
// Example of a for loop:
for (var counter = 1; counter < 11; counter++) {
	console.log(counter);
}



//Starting the for loop
// Change where the for loop starts.
for (var i = 5; i < 11; i = i + 1){
	console.log(i);
}




//Ending the for loop
// Edit this for loop
for (var i = 4; i < 24; i = i + 1) {
	console.log(i);
}



//Controlling the for loop
// Edit this for loop!
for (var i = 5; i <= 50; i+=5) {
	console.log(i);
}



//How does it work
// Example for loop
for (var i = 8 ; i < 120; i+=12) {
	console.log(i);
}




//Practice counting down
// Example of infinite loop. THIS WILL CRASH YOUR
// BROWSER. Don't run the code without changing it!
for (var i = 10; i >= 0; i--) {
	console.log(i);
}



//Last practice for loop
for (var i=100; i>0; i-=5){
	console.log(i);
}



//Meet arrays
var junk= ["old guitar", "broken amp",0,10];
console.log(junk);



//Array positions
var junkData = ["Eddie Murphy", 49, "peanuts", 31];
console.log(junkData[3]);


//Loops and arrays 1
// Let's print out every element of an array using a for loop
var cities = ["Melbourne", "Amman", "Helsinki", "NYC", "Makati", "Cagayan de Oro"];

for (var i = 0; i < cities.length; i++) {
    console.log("I would like to visit " + cities[i]);
}




//Loops and arrays 2
var names=["minotaur","kobold","troll","demigod","hill orc"];
for (var i=0; i< names.length; i++){
	console.log("I know someone called "+ names[i]);
}