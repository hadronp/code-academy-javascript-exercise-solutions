// defining an array
var list=["battleaxe","giant spiked club","bastard sword","dagger"];


// Access by offset
var languages = ["HTML", "CSS", "JavaScript", "Python", "Ruby"];
console.log(languages[2]);


// Array properties
var languages = ["HTML", "CSS", "JavaScript", "Python", "Ruby"];
console.log(languages[2]);
console.log(languages.length);


//Iterating over an array
var languages = ["HTML", "CSS", "JavaScript", "Python", "Ruby"];

for (i=0; i< languages.length; i++){
	console.log(languages[i]);
}


// Heterogeneous arrays
var myArray=[1,true,"3","blades","potions"];


//Arrays of arrays
var newArray=[[1,2,3],[4,5,6],[7,8,9]];


// Jagged arrays
var jagged= [[1,2,3],[4,5],6,7,8,9];



//nouns and verbs together
var phonebookEntry = {};

phonebookEntry.name = 'Oxnard Montalvo';
phonebookEntry.number = '(555) 555-5555';
phonebookEntry.phone = function() {
  console.log('Calling ' + this.name + ' at ' + this.number + '...');
};

phonebookEntry.phone();


//Object Syntax
var me={
	name: "Aryan Lowell",
	age: 33
};



// Craeting a new object
var me= new Object();
me.name="Aryan";
me.age=33;



//Practice makes perfect
var object1= new Object();
var object2= new Object();
var object3= new Object();

object1.name="Maxitaur";
object1.weapon="Poisoned Dagger";

object2.name="Basherbane";
object2.weapon="Giant Spiked Club";

object3.name="BrontoCentaur";
object3.weapon="Heavy Ice Crossbow";




// Heterogeneous arrays 
var me= new Object();
me.name="Aryan";
me.age=33;

var myArray=[1,true,"Trident",me]



//Multidimensional arrays
var me= new Object();
me.name="Aryan";
me.age=33;
var newArray=[[1,2,3,4],[5,me]];




//Editing an existing object
var myObject = {
  name: 'Eduardo',
  type: 'Most excellent',
  // Add your code here!
  interests: ["bowling","sky diving","eating"]
  
};




//Creating your own objects
var myOwnObject= new Object();
myOwnObject.name= "Spartacus";
myOwnObject.country= "Greece"
myOwnObject.occupation= "Warrior";

