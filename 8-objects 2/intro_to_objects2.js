// An Objective review
 var james = {
    job: "programmer",
    married: false,
};

function Person(job, married) {
    this.job = job;
    this.married = married;
};

// create a "gabby" object using the Person constructor!
var gabby=new Person("student", true);



//Fun with functions
function Person(job, married) {
    this.job = job;
    this.married = married;
    // add a "speak" method to Person!

    this.speak= function(){
    	console.log("Hello!");
    };
    
}

var user = new Person("Codecademy Student",false);
user.speak();




//Literally Speaking
var james = {
    job: "programmer",
    married: false,
    speak: function(mood) {
      	console.log("Hello, I am feeling " + mood);
      	console.log("Hello, I am feeling " + mood);  
    }
};

james.speak("great");
james.speak("just okay");





//Can I see your references
var james = {
    job: "programmer",
    married: false,
    sayJob: function() {
        console.log("Hi, I work as a " + this.job);
        
    }
};

// james' first job
james.sayJob("programmer");

// change james' job to "super programmer" here
james.job="super programmer";

// james' second job
james.sayJob();





//Who's in your bracket
var james = {
    job: "programmer",
    married: false
};

// set to the first property name of "james"
var aProperty = "job"

// print the value of the first property of "james" 
// using the variable "aProperty"
console.log(james[aProperty]);




//I.D., Please
// complete these definitions so that they will have
// the appropriate types
var anObj = { job: "I'm an object!" };
var aNumber = 42;
var aString = "I'm a string!";

console.log(typeof anObj); // should print "object"
console.log(typeof aNumber); // should print "number"
console.log(typeof aString); // should print "string"




//Know thyself
var myObj = {
    // finish myObj
    name: "minotaur"
    
};

console.log( myObj.hasOwnProperty('name') ); // should print true
console.log( myObj.hasOwnProperty('nickname') ); // should print false




//Dressed to impress
var suitcase = {
    shirt: "Hawaiian"
};

if(suitcase.hasOwnProperty("shorts")){
	console.log(suitcase.shorts);
}
else{
	suitcase.shorts ="board shorts"; 
}

console.log(suitcase.shorts);




//Getting I-timate
var nyc = {
    fullName: "New York City",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

for( var property in nyc){
	console.log(property);
}





//List ALL the properties
var nyc = {
    fullName: "New York City",
    mayor: "Bill de Blasio",
    population: 8000000,
    boroughs: 5
};

// write a for-in loop to print the value of nyc's properties
for (var property in nyc){
	console.log(nyc[property]);
}




//Class is in session
function Person(name,age) {
  this.name = name;
  this.age = age;
}

// Let's make bob again, using our constructor
var bob = new Person("Bob Smith", 30);
var susan = new Person("Susan Jordan", 35);

// make your own class here
function Circle(radius){
	this.radius=radius;
}




//Teach Snoopy
function Dog (breed) {
  this.breed = breed;
}

// here we make buddy and teach him how to bark
var buddy = new Dog("Golden Retriever");
buddy.bark = function() {
  console.log("Woof");
};
buddy.bark();

// here we make snoopy
var snoopy = new Dog("Beagle");
// we need you to teach snoopy how to bark here
snoopy.bark = function() {
  console.log("Aarf");
};
// this causes an error, because snoopy doesn't know how to bark!
snoopy.bark();




//How do classes help us
function Person(name,age) {
  this.name = name;
  this.age = age;
}
// a function that prints the name of any given person
var printPersonName = function (p) {
  console.log(p.name);
};

var bob = new Person("Bob Smith", 30);
printPersonName(bob);

// make a person called me with your name and age
// then use printPersonName to print your name
var me= new Person("Human", 33);
console.log(printPersonName(me));





//Prototype to the rescue
function Dog (breed) {
  this.breed = breed;
};

// here we make buddy and teach him how to bark
var buddy = new Dog("golden Retriever");
Dog.prototype.bark = function() {
  console.log("Woof");
};
buddy.bark();

// here we make snoopy
var snoopy = new Dog("Beagle");
/// this time it works!
snoopy.bark();



//Prototype practice
function Cat(name, breed) {
    this.name = name;
    this.breed = breed;
}

// let's make some cats!
var cheshire = new Cat("Cheshire Cat", "British Shorthair");
var gary = new Cat("Gary", "Domestic Shorthair");

// add a method "meow" to the Cat class that will allow
// all cats to print "Meow!" to the console
Cat.prototype.meow= function(){
	console.log("Meow!");
}

// add code here to make the cats meow!
cheshire.meow;
gary.meow;





//It's all in the Genes
// create your Animal class here
function Animal(name, numLegs){
	this.name=name;
	this.numLegs=numLegs;
}


// create the sayName method for Animal
Animal.prototype.sayName= function(){
	console.log("Hi my name is " + this.name);
}

// provided code to test above constructor and method
var penguin = new Animal("Captain Cook", 2);
penguin.sayName();





//Marching Penguins
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Hi my name is " + this.name);
}


// create a Penguin constructor here
function Penguin(name, numLegs){
	this.name = name;
    this.numLegs = numLegs;
}


// create a sayName method for Penguins here
Penguin.prototype.sayName = function() {
    console.log("Hi my name is " + this.name);
}


// our test code
var theCaptain = new Penguin("Captain Cook", 2);
theCaptain.sayName();





//DRY Penguins
// the original Animal class and sayName method
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Hi my name is " + this.name);
};

// define a Penguin class
function Penguin(name){
	this.name=name;
	this.numLegs=2;
}

// set its prototype to be a new instance of Animal
Penguin.prototype=new Animal();




//Black (and white) Penguin Magic
// the original Animal class and sayName method
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
}
Animal.prototype.sayName = function() {
    console.log("Hi my name is " + this.name);
};

// define a Penguin class
function Penguin(name){
	this.name=name;
	this.numLegs=2;
}

// set its prototype to be a new instance of Animal
Penguin.prototype=new Animal();
var penguin=new Penguin("Fluffy");
console.log(penguin.sayName);




//Penguins, Properties and the Prototype
function Penguin(name) {
    this.name = name;
    this.numLegs = 2;
}

// create your Emperor class here and make it inherit from Penguin
function Emperor(name){
	this.name = name;
}

Emperor.prototype=new Penguin();

// create an "emperor" object and print the number of legs it has
var emperor=new Emperor("Brandy");
console.log(emperor.numLegs);





//Up the Foof-I-mean-Protorype Chain
// original classes
function Animal(name, numLegs) {
    this.name = name;
    this.numLegs = numLegs;
    this.isAlive = true;
}
function Penguin(name) {
    this.name = name;
    this.numLegs = 2;
}
function Emperor(name) {
    this.name = name;
    this.saying = "Waddle waddle";
}

// set up the prototype chain
Penguin.prototype = new Animal();
Emperor.prototype = new Penguin();

var myEmperor = new Emperor("Jules");

console.log(myEmperor.saying); // should print "Waddle waddle"
console.log(myEmperor.numLegs); // should print 2
console.log(myEmperor.isAlive); // should print true




//Open to the public
function Person(first,last,age) {
   this.firstName = first;
   this.lastName = last;
   this.age = age;
}

var john = new Person('John','Smith',30);
var myFirst = john.firstName;
var myLast = john.lastName;

//declare variable myAge set to the age of the john object.
var myAge=john.age;




//Private Variables
function Person(first,last,age) {
   this.firstname = first;
   this.lastname = last;
   this.age = age;
   var bankBalance = 7500;
}

// create your Person 
var john = new Person ("John" ,"Kalang");

// try to print his bankBalance
console.log(john.bankBalance);




//Acessing Private Variables
function Person(first,last,age) {
   this.firstname = first;
   this.lastname = last;
   this.age = age;
   var bankBalance = 7500;
  
   this.getBalance = function() {
      // your code should return the bankBalance
      return bankBalance;
      
   };
}

var john = new Person('John','Smith',30);
console.log(john.bankBalance);

// create a new variable myBalance that calls getBalance()
var myBalance= john.getBalance();
console.log(myBalance);





//Private Methods
function Person(first,last,age) {
   this.firstname = first;
   this.lastname = last;
   this.age = age;
   var bankBalance = 7500;
  
   var returnBalance = function() {
      return bankBalance;
   };
       
   // create the new function here
   this.askTeller = function(){
   		return returnBalance;
   }
   
}

var john = new Person('John','Smith',30);
console.log(john.returnBalance);
var myBalanceMethod = john.askTeller();
var myBalance = myBalanceMethod();
console.log(myBalance);





//Passing Arguments
function Person(first,last,age) {
   this.firstname = first;
   this.lastname = last;
   this.age = age;
   var bankBalance = 7500;
  
   this.askTeller = function(pass) {
     if (pass == 1234) return bankBalance;
     else return "Wrong password.";
   };
}

var john = new Person('John','Smith',30);
/* the variable myBalance should access askTeller()
   with a password as an argument  */
var myBalance= john.askTeller(1234);




//Looks For-In to me
var languages = {
    english: "Hello!",
    french: "Bonjour!",
    notALanguage: 4,
    spanish: "Hola!"
};

// print hello in the 3 different languages
for (var greet in languages){
	if( typeof languages[greet] === "string"){
		console.log(languages[greet]);	
	}
}




//Hello? Yes, this is a Dog
function Dog (breed) {
    this.breed = breed;
};

// add the sayHello method to the Dog class 
// so all dogs now can say hello
Dog.prototype.sayHello= function(){
	console.log("Hello this is a " + this.breed + " dog");
};

var yourDog = new Dog("golden retriever");
yourDog.sayHello();

var myDog = new Dog("dachshund");
myDog.sayHello();




//So Meta I Can't Take It!
// what is this "Object.prototype" anyway...?
var prototypeType = typeof Object.prototype;
console.log(prototypeType);

// now let's examine it!
var hasOwn =Object.prototype.hasOwnProperty("hasOwnProperty");
console.log(hasOwn);




//Private Eye
function StudentReport() {
    var grade1 = 4;
    var grade2 = 2;
    var grade3 = 1;
    this.getGPA = function() {
        return (grade1 + grade2 + grade3) / 3;
    };
}

var myStudentReport = new StudentReport();

for(var x in myStudentReport) {
    if(typeof myStudentReport[x] !== "function") {
        console.log("Muahaha! " + myStudentReport[x]);
    }
}

console.log("Your overall GPA is " + myStudentReport.getGPA());
