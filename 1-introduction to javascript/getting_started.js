//confirm('i feel flawsome')
//prompt("What is your name?")
//"The quick brown fox jumps over the lazy dog".length

/* Boleans

"I'm coding like a champ".length > 10
*/

/* console.log 

console.log(2 * 5)
console.log("Hello")
*/

/* Comparisons

// Here is an example of using the greater than (>) operator.
console.log(15 > 4); // 15 > 4 evaluates to true, so true is printed.

// Fill in with >, <, === so that the following print out true:
console.log("Xiao Hui".length <  122);
console.log("Goody Donaldson".length >  8);
console.log(8*2  ==== 16);
*/

/* Decisions, decisions

if ( "Trolls are flawsome".length < 20 ) {
    console.log("not flawsome enough" );
}
*/

/*Computers are smart

if ("A troll says EAT" ) 
{
    console.log("Let's go down the first road!");
}
else 
{
    // What should we do if the condition is false? Fill in here:
    console.log("Lets go down the second road");
}
*/

/*More practice with conditionals

if ("Troll has a massive club".length < 10) 
{
    // if condition is true
    console.log("The condition is true");
}
else // "otherwise"
{
    // do this code instead
    console.log("The condition is false");
}
*/

/* Computers aren't that smart 
if (10 === 10) {
    console.log("You got a true!");
} else {
    console.log("You got a false!");
}
*/

/* Mid-lesson breather
// This is an example of an if / else statement.

if (12 / 4 === "Ari".length) {
    confirm("Will this run the first block?");
} else {
    confirm("Or the second block?");
}
*/

/* Math
 if ("Jon".length * 2 / (2+1) === 2)
{
    console.log("The answer makes sense!");
} 
else {
	console.log("Error Error Error");
}
*/

/* Math and the modulo 
console.log(14 % 3);
console.log(99 % 8);
console.log(11 % 3);
*/

/* Modulo and if/else
if( 10 % 2 == 0 ) {
    console.log("The first number is even");
} else {
    console.log("The first number is odd");
}
*/

/* Substrings 
"wonderful day".substring(3,7);
*/

/* More substring practice 
console.log("January".substring(0,3));
console.log("Melbourne is great".substring(0,12));
console.log("Hamburgers".substring(3,10));
*/

/* Variables 
myAge= 33;
console.log(myAge);
*/


/* More Variable Practice
// Declare a variable on line 3 called
// myCountry and give it a string value.
myCountry="Philippines";

// Use console.log to print out the length of the variable myCountry.
console.log(myCountry.length);

// Use console.log to print out the first three letters of myCountry.
console.log(myCountry.substring(0,3));
*/

/* Change variable values
// On line 2, declare a variable myName and give it your name.
myName="Aryan Lowell";

// On line 4, use console.log to print out the myName variable.
console.log(myName);

// On line 7, change the value of myName to be just the first 2 
// letters of your name.
myName=myName.substring(0,2);

// On line 9, use console.log to print out the myName variable.
console.log(myName);
*/
